import { useState, useEffect } from "react";
import "./App.css";
import ProductList from "./Components/ProductList/ProductList";
import FetchProducts from "./Components/FetchProducts/FetchProducts";


function App() {
  const [products, setProducts] = useState([]);
  const [searchValue, setSearchValue] = useState("");
  const [foundedProducts, setFoundedProducts] = useState([]);


  useEffect(() => {
    setFoundedProducts(products);
  }, [products]);

  const titles = [];
 
  if (products.length > 0) {
    for (let i = 0; i < products.length; i++) {
      titles.push({
        title: products[i].title.toLowerCase(),
        id: products[i].id,
      });
    }
  }


  const foundSearchIds = [];
  if (searchValue.length >= 3) {
    titles.forEach((item) => {
      if (item.title.includes(searchValue.toLowerCase())) {
        foundSearchIds.push(item.id);
      }
    });
  }

  let tempFoundProducts = [];
  products.forEach((item) => {
    if (foundSearchIds.includes(item.id)) {
      tempFoundProducts.push(item);
    }
  });

  useEffect(() => {
    if (searchValue.length >= 3) {
      setFoundedProducts([...tempFoundProducts]);
    } else {
      setFoundedProducts(products);
    }
  }, [searchValue]);


  const categories = ["Все товары"];
 
  if (products.length > 0) {
    for (let i = 0; i < products.length; i++) {
      if (!categories.includes(products[i].category)) {
        categories.push(products[i].category);
      }
    }
  }

  const [selectValue, setSelectValue] = useState("Все товары");

 
  let tempFoundProductsFromSelect = [];
  products.forEach((item) => {
    if (item.category === selectValue) {
      tempFoundProductsFromSelect.push(item);
    }
  });

  useEffect(() => {
    if (selectValue !== "Все товары") {
      setFoundedProducts([...tempFoundProductsFromSelect]);
    } else {
      setFoundedProducts(products);
    }
  }, [selectValue]);

  return (
    <div className="App">
      <FetchProducts setProducts={setProducts} />

      <ProductList
        products={foundedProducts}
        searchValue={searchValue}
        setSearchValue={setSearchValue}
        categories={categories}
        selectValue={selectValue}
        setSelectValue={setSelectValue}
      />

    </div>
  );
}

  
 
export default App;
